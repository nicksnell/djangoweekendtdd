from django.test import TestCase
from django.http import HttpRequest

from lists.views import homepage

class TestStuff(TestCase):

	def test_homepage_returns_html_with_todo_in_title(self):
		request = HttpRequest()
		response = homepage(request)

		self.assertTrue(response.content.startswith(b'<html>'))
		self.assertIn('<title>To-Do lists</title>', response.content.decode())
		self.assertTrue(response.content.strip().endswith(b'</html>'))